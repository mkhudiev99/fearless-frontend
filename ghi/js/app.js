function createCard(name, description, pictureUrl, starts, ends, location) {
    return `
      <div class="card">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <p class="card-subtitle mb-2 text-muted">${location}</p>
          <p class="card-text">${description}</p>
          <p class="card-footer">${starts} - ${ends}</p>
        </div>
      </div>
    `;
  }

window.addEventListener('DOMContentLoaded', async () => {

const url = 'http://localhost:8000/api/conferences/';

try {
    const response = await fetch(url);

    if (!response.ok) {
    // Figure out what to do when the response is bad
    } else {
    const data = await response.json();
    // const conferences = data.conferences.;
    const nameTag = document.querySelector('.card-title');
    let index = 0
    for (let conference of data.conferences) {
        const detailUrl = `http://localhost:8000${conference.href}`;
        const detailResponse = await fetch(detailUrl);
        if (detailResponse.ok) {
            const details = await detailResponse.json();
            const title = details.conference.name;
            const starts = details.conference.starts
            const ends = details.conference.ends
            const location = details.conference.location.name
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            const html = createCard(title, description, pictureUrl, starts, ends, location);
            const column = document.querySelector(`#col-${index % 3}`);
            column.innerHTML += html;
            index += 1
        }
    }

    }
    } catch (e) {
        console.error("You got an error");
        const newHTML = alertComponent()
        const somethingWrong = document.querySelector('something-wrong')
        somethingWrong.innerHTML = newHTML
        // Figure out what to do if an error is raised
    }

});

// window.addEventListener('DOMContentLoaded', async () => {

//   const url = 'http://localhost:8000/api/conferences/';

//   try {
//     const response = await fetch(url);

//     if (!response.ok) {
//       // Figure out what to do when the response is bad
//     } else {
//       const data = await response.json();

//       for (let conference of data.conferences) {
//         const detailUrl = `http://localhost:8000${conference.href}`;
//         const detailResponse = await fetch(detailUrl);
//         if (detailResponse.ok) {
//           const details = await detailResponse.json();
//           const title = details.conference.title;
//           const description = details.conference.description;
//           const pictureUrl = details.conference.location.picture_url;
//           const html = createCard(title, description, pictureUrl);
//           console.log(html);
//         }
//       }

//     }
//   } catch (e) {
//     // Figure out what to do if an error is raised
//   }

// });



// window.addEventListener('DOMContentLoaded', async () => {
//     const url = 'http://localhost:8000/api/conferences/';
//     try {
//         const response = await fetch(url);
    
//         if (!response.ok) {
//           // Figure out what to do when the response is bad
//           throw new Error("Response not okay")
//         } else {
//           const data = await response.json();
//         }
//       } catch (e) {
//         // Figure out what to do if an error is raised
//       }
    
//     });
    // const response = await fetch(url);
    // console.log(response);

    // const data = await response.json();
    // console.log(data);
// });
