

window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/locations/';

    const response = await fetch(url);


    if (response.ok) {
        console.log("response",response);
        const data = await response.json();
        console.log("data",data);

        const selectTag = document.getElementById('location');
        for (let loc of data.locations){
            let option = document.createElement('option');
            option.value = loc.id;
            option.innerHTML = loc.name;
            selectTag.appendChild(option);

        }
            // State fetching code, here...
        
        const formTag = document.getElementById('create-conference-form');
        formTag.addEventListener('submit', async event => {
        event.preventDefault();

        
        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));

        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: "post",
            body: json,
            headers: {
                'Content-Type': 'application/json',
    },
    };
    const response = await fetch(conferenceUrl, fetchConfig);
    if (response.ok) {
    formTag.reset();
    const newLocation = await response.json();
    }

        

        });
    }});

//     } else {
//         console.error('Got an error in the response.')
//     }
// });




     // Create an 'option' element

      // Set the '.value' property of the option element to the
      // state's abbreviation

      // Set the '.innerHTML' property of the option element to
      // the state's name

      // Append the option element as a child of the select tag
