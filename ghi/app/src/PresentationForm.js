import React from "react";

class PresentationForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            presenterName: '',
            presenterEmail: '',
            companyName: '',
            title: '',
            synopsis: '',
            conferences: [],
        }

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleEmailChange = this.handleEmailChange.bind(this);
        this.handleCompanyChange = this.handleCompanyChange.bind(this);
        this.handleTitleChange = this.handleTitleChange.bind(this);
        this.handleSynopsisChange = this.handleSynopsisChange.bind(this);
        this.handleConferenceChange = this.handleConferenceChange.bind(this);

    }

    async componentDidMount() {
        const url = 'http://localhost:8000/api/conferences/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            this.setState({ conferences: data.conferences });
        }
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = { ...this.state };
        data.presenter_name = data.presenterName;
        data.presenter_email = data.presenterEmail;
        data.company_name = data.CompanyName;
        delete data.presenterName;
        delete data.presenterEmail;
        delete data.CompanyName;
        delete data.conferences;
        console.log(data);


        const selectTag = document.getElementById('conference');
        const conferenceId = selectTag.options[selectTag.selectedIndex].value;
        const presentationUrl = `http://localhost:8001/api/conferences/${conferenceId}/presentations/`;
        const fetchOptions = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const presentationResponse = await fetch(presentationUrl, fetchOptions);
        if (presentationResponse.ok) {
            const newPresentation = await presentationResponse.json();
            console.log(newPresentation)
        }

        const cleared = {
            presenterName: '',
            presenterEmail: '',
            companyName: '',
            title: '',
            synopsis: '',
            conferences: [],
        }
        this.setState(cleared)
    }

    handleNameChange(event) {
        const value = event.target.value;
        this.setState({ presenterName: value });
    }

    handleEmailChange(event) {
        const value = event.target.value;
        this.setState({ presenterEmail: value });
    }

    handleCompanyChange(event) {
        const value = event.target.value;
        this.setState({ companyName: value });
    }

    handleTitleChange(event) {
        const value = event.target.value;
        this.setState({ title: value });
    }

    handleSynopsisChange(event) {
        const value = event.target.value;
        this.setState({ synopsis: value });
    }

    handleConferenceChange(event) {
        const value = event.target.value;
        this.setState({ conferences: value });
    }

    render() {
        let spinnerClasses = 'd-flex justify-content-center mb-3';
        let dropdownClasses = 'form-select d-none';
        if (this.state.conferences.length > 0) {
          spinnerClasses = 'd-flex justify-content-center mb-3 d-none';
          dropdownClasses = 'form-select';
        }
    
        let messageClasses = 'alert alert-success d-none mb-0';
        let formClasses = '';
        if (this.state.hasSignedUp) {
          messageClasses = 'alert alert-success mb-0';
          formClasses = 'd-none';
        }
    
            return (
                <div className="row">
                    <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create a new presentation</h1>
                        <form onSubmit={this.handleSubmit} id="create-presentation-form">
                        <div className="form-floating mb-3">
                            <input onChange={this.handleNameChange} placeholder="Presenter name" required type="text" name="presenter_name" id="presenter_name" className="form-control"/>
                            <label htmlFor="presenter_name">Presenter name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={this.handleEmailChange} placeholder="Presenter email" required type="email" name="presenter_email" id="presenter_email" className="form-control"/>
                            <label htmlFor="presenter_email">Presenter email</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={this.handleCompanyChange} placeholder="Company name" type="text" name="company_name" id="company_name" className="form-control"/>
                            <label htmlFor="company_name">Company name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={this.handleTitleChange} placeholder="Title" required type="text" name="title" id="title" className="form-control"/>
                            <label htmlFor="title">Title</label>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="synopsis">Synopsis</label>
                            <textarea onChange={this.handleSynopsisChange} className="form-control" id="synopsis" rows="3" name="synopsis"></textarea>
                        </div>
                        <div className="mb-3">
                            <select onChange={this.handleConferenceChange} required name="conference" id="conference" className="form-select">
                            <option value="">Choose a conference</option>
                            {this.state.conferences.map(conference => {
                                return (
                                    <option key={conference.href} value={conference.href}>{conference.name}</option>
                                )
                            })} */}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                    </div>
                </div>
        );
      }
}

export default PresentationForm;